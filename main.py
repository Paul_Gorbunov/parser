from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver import ActionChains
from time import sleep
import datetime, time
import re
import openpyxl
import os


class Parser:
    def __init__(self):
        adress = "https://popsters.ru/app/start"
        location = os.path.abspath(os.getcwd())+"/geckodriver"
        self.driver = webdriver.Firefox(executable_path=location)
        self.driver.get(adress)
        self.main_page = self.driver.current_window_handle 
        self.logged = {"tw":0,"vk":0,"fb":0,"ok":0,"inst":0}
        try :
            os.mkdir("data")
        except:
            pass


    def check_login(self,net,log_p,name):
        sleep(5)
        if not "active" in self.driver.find_element_by_css_selector("li[social='"+name+"'").get_attribute("class"):
            net(log_p)
        else:
            print("logged "+name)
            
    @staticmethod
    def passer(func):
        def wrap(*a):
            try :
                return func(*a)
            except:
                return []
        return wrap
    
    #@Parser.passer
    def __search(self,link,date):
        sleep(15)
        ob = self.driver.find_element_by_css_selector("textarea[id='input-url-search']")
        ob.clear()
        ob.send_keys(link)
        sleep(5)
        self.driver.find_element_by_css_selector("button[id='search-page-btn']").click()
        sleep(5)
        self.driver.find_element_by_css_selector("button[class='btn btn-default btn-sm ng-binding']").click()
        sleep(5)
        self.driver.find_element_by_css_selector("a[class='btn']").click()
        sleep(5)
        cal = self.driver.find_element_by_css_selector("input[class='form-control ng-isolate-scope']")
        cal.clear()
        [cal.send_keys(Keys.BACKSPACE) for i in range(32)]
        cal.send_keys(date)
        self.driver.find_element_by_css_selector("button[class='btn bg-pink btn-lg btn-block ng-binding']").click()
        sleep(15)
        self.driver.find_element_by_css_selector("a[class='compare-table ng-scope']").click()
        sleep(8)
        text  = self.driver.page_source
        self.driver.save_screenshot("data/"+link.split("/")[2]+"_"+link.split("/")[-1]+"_"+date+".png")
        self.driver.refresh()
        return [re.findall("[0-9 %.]+",u)[1] for u in re.findall('"points-data ng-binding">[0-9 %.]+<',text)[3:6]]

    def log(self,l):
        if "vk" in l and self.logged['vk']:
            return 1
        elif "twit" in l and self.logged['tw']:
            return 1
        elif "inst" in l and self.logged['inst']:
            return 1
        elif "face" in l and self.logged['fb']:
            return 1
        elif "ok" in l and self.logged['ok']:
            return 1
        else:
            return 0

    def _search(self,num,name,link,date):
        if  link != None and self.log(link):
            self.write_xlsx(num,name,self.__search(link,date),link)
            print(link,date)
            return 0
        else:
            #print("error in link or data")
            return 1
        
    def write_xlsx(self,k,name,text,link):
        try :
            workbook = openpyxl.load_workbook("smth.xlsx") 
        except FileNotFoundError:
            workbook = openpyxl.Workbook()
        sheet = workbook.get_sheet_by_name(name)
        sheet.cell(row = k, column = 3).value = link
        sheet.cell(row = k, column = 4).value = text[0]
        sheet.cell(row = k, column = 6).value = text[1]
        sheet.cell(row = k, column = 8).value = text[2]
        workbook.save("smth.xlsx")
        
    def login_tw(self,login,password):
        self.driver.find_element_by_css_selector("li[social='tw']").click()
        login_page = [handle for handle in self.driver.window_handles if handle != self.main_page][0]
        self.driver.switch_to.window(login_page)
        sleep(6)
        self.driver.find_element_by_css_selector("input[id='username_or_email']").send_keys(login)
        sleep(1)
        self.driver.find_element_by_css_selector("input[id='password']").send_keys(password)
        sleep(1)
        self.driver.find_element_by_css_selector("input[id='allow']").click()
        sleep(8)
        self.driver.switch_to.window(self.main_page)
        self.check_login(self.get_code_tw,login_page,"tw")
        self.logged["tw"] = 1
        
    
    def get_code_tw(self,login_page):
        code = input("Input code tw: ")
        self.driver.switch_to.window(login_page)
        sleep(4)
        self.driver.find_element_by_css_selector("input[id='challenge_response']").send_keys(code)
        self.driver.find_element_by_css_selector("input[id='email_challenge_submit']").click()
        sleep(4)
        self.driver.find_element_by_css_selector("input[id='allow']").click()
        print("logged tw")
        sleep(15)
        self.driver.switch_to.window(self.main_page)
        
    def login_vk(self,login,password):
        self.driver.find_element_by_css_selector("li[social='vk']").click()
        login_page = [handle for handle in self.driver.window_handles if handle != self.main_page][0]
        self.driver.switch_to.window(login_page)
        sleep(6)
        self.driver.find_element_by_css_selector("input[name='email']").send_keys(login)
        sleep(1)
        self.driver.find_element_by_css_selector("input[name='pass']").send_keys(password)
        sleep(1)
        self.driver.find_element_by_css_selector("button[id='install_allow']").click()
        sleep(8)
        self.driver.switch_to.window(self.main_page)
        self.check_login(self.get_code_vk,login_page,"vk")
        self.logged["vk"] = 1
        
    def get_code_vk(self,login_page):
        code = input("Input code vk: ")
        self.driver.switch_to.window(login_page)
        sleep(4)
        self.driver.switch_to.window(self.main_page)
                
    def login_fb(self,login,password):
        self.driver.find_element_by_css_selector("li[social='facebook']").click()
        login_page = [handle for handle in self.driver.window_handles if handle != self.main_page][0]
        self.driver.switch_to.window(login_page)
        sleep(6)
        self.driver.find_element_by_css_selector("input[id='email']").send_keys(login)
        sleep(1)
        self.driver.find_element_by_css_selector("input[id='pass']").send_keys(password)
        sleep(1)
        self.driver.find_element_by_css_selector("button[id='loginbutton']").click()
        sleep(8)
        self.driver.switch_to.window(self.main_page)
        self.check_login(self.get_code_fb,login_page,"facebook")
        self.logged["fb"] = 1
    
    def get_code_fb(self,login_page):
        code = input("Input code fb: ")
        self.driver.switch_to.window(login_page)
        sleep(4)
        self.driver.switch_to.window(self.main_page)
    
    def login_inst(self,login,password):
        self.driver.find_element_by_css_selector("li[social='inst']").click()
        login_page = [handle for handle in self.driver.window_handles if handle != self.main_page][0]
        self.driver.switch_to.window(login_page)
        sleep(6)
        self.driver.find_element_by_css_selector("input[name='username']").send_keys(login)
        sleep(1)
        self.driver.find_element_by_css_selector("input[name='password']").send_keys(password)
        sleep(1)
        self.driver.find_element_by_css_selector("button[type='submit']").click()
        sleep(8)
        self.driver.switch_to.window(self.main_page)
        self.check_login(self.get_code_inst,login_page,"inst")
        self.logged["inst"] = 1
        
    def get_code_inst(self,login_page):
        code = input("Input code inst: ")
        self.driver.switch_to.window(login_page)
        sleep(15)
        self.driver.switch_to.window(self.main_page)
    
    def login_ok(self,login,password):
        self.driver.find_element_by_css_selector("li[social='odn']").click()
        login_page = [handle for handle in self.driver.window_handles if handle != self.main_page][0]
        self.driver.switch_to.window(login_page)
        sleep(6)
        self.driver.find_element_by_css_selector("input[id='field_email']").send_keys(login)
        sleep(2)
        self.driver.find_element_by_css_selector("input[id='field_password']").send_keys(password)
        sleep(2)
        self.driver.find_element_by_css_selector("input[value='Sign in']").click()
        sleep(8)
        try:
            self.driver.find_element_by_css_selector("button[name='button_accept_request']").click()
            sleep(3)
        except:
            pass
        self.driver.switch_to.window(self.main_page)
        self.check_login(self.get_code_ok,login_page,"odn")
        self.logged["ok"] = 1
    
    def get_code_ok(self,login_page):
        code = input("Input code odn: ")
        self.driver.switch_to.window(login_page)
        sleep(15)
        self.driver.switch_to.window(self.main_page)
    
        
    def log_all(self,lis):
        try :
            workbook = openpyxl.load_workbook("input.xlsx") 
        except FileNotFoundError:
            return 1
        sheet = workbook.active
        l = [[sheet.cell(row=2,column=num[0]).value,sheet.cell(row=3,column=num[0]).value] for num in lis]
        try :
            [lis[u][1](self,l[u][0],l[u][1]) for u in range(len(lis))]
        except FileNotFoundError:
            return 1
        return 0
    
    def start(self,names):
        try :
            workbook = openpyxl.load_workbook("smth.xlsx") 
        except FileNotFoundError:
            return 1
        ret = []
        for q in names :
            sheet = workbook.get_sheet_by_name(q)
            def get_(u):
                try:
                    return [sheet.cell(row = u, column = 3).value,sheet.cell(row = u, column = 11).value]
                except:
                    return [None,None]
            ret.append([self._search(u,q,*get_(u)) for u in range(2,sheet.max_row+1)])
        return ret

    
    def close(self):
        self.driver.close()
    
        
obj = Parser()
print("starting")
lis = [(4,Parser.login_inst),(3,Parser.login_fb),(2,Parser.login_vk)],(1,Parser.login_tw,Parser),(5,Parser.login_ok)]
if obj.log_all(lis) == 1:
    print("error in input.xlsx file login information")
else :
    print("logged")
obj.start(["ВКонтакте","Facebook","Instagram","Twitter","Одноклассники"])
print("ok")




