Сервер Ubuntu

Отправить файлы:

scp -r / home / local / my / script root@server.my: / home / dir / script

Подключиться через ssh:

ssh username@ssh.server.com
введите пароль

После входа на сервер через ssh:

установить библиотеки:


sudo apt-get -y install python3-pip
sudo apt-get update
sudo apt-get upgrade
sudo pip3 install selenium 
sudo pip3 install datetime
sudo pip3 install openpyxl

перейдите в каталог скрипта:

cd / home / local / my / script

Запустить:

python3 main.py

дождаться "starting" сообщения

Если появится сообщение "Input code ...:", введите код из соц. сети

Сообщение "logged" значит, что все сети к которым был доступ залогинены

Сообщение "ок" означает, что все ссылки проверены

Получить файлы обратно:

scp -r root@server.my: / home / dir / script / data / home / local / my /
